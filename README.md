﻿
## 一个用于app指向性功能高亮的库

## 演示
![image](https://gitee.com/openharmony-tpc/Highlight/raw/master/screenshot/showTip.png)


## 集成
```
方式一：
通过highlight library生成har包，添加har包到要集成的libs文件夹内
在entry的gradle内添加如下代码
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation project(':highlight')
}

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:highlight:1.0.4'
``````

## entry运行要求
   sdk5模拟器运行需要配置apiVersion字段：
         "compatible": 5,
         "target": 5,
         "releaseType": "Beta1"

   将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## 示例
```java
mHightLight =
                new HighLight(MainAbility.this)
                        .autoRemove(false)
                        .intercept(true) // 设置拦截点击事件属性，设置为true,高亮布局拦截监听点击事件
    					.maskColor(0xCC003300) // 动态设置蒙版颜色
    					.enableNext() // 允许按次序显示引导布局
                        .setClickCallback(
                                new HighLight.OnClickCallback() {
                                    @Override
                                    public void onClick() {
                                        remove();
                                    }
                                })
                        .anchor(rootLayout) // 可不设置，由程序自动获取页面根布局。
                        // 在高亮按钮2 右边显示高亮提示布局
                        .addHighLight(
                                ResourceTable.Id_btn_light2,
                                ResourceTable.Layout_info_down,
                                new OnRightPosCallback(40),
                                new RectLightShape())
                        // 在高亮按钮1 左边显示高亮提示布局
                        .addHighLight(
                                ResourceTable.Id_btn_light1,
                                ResourceTable.Layout_info_down,
                                new OnLeftPosCallback(-150),
                                new CircleLightShape(0, 0, 0))
                        // 在高亮按钮3 头顶显示高亮提示布局
                        .addHighLight(
                                ResourceTable.Id_btn_light3,
                                ResourceTable.Layout_info_down,
                                new OnTopPosCallback(-150),
                                new CircleLightShape())
                        // 在showKnownTip 底部显示高亮提示布局
                        .addHighLight(
                                view,
                                ResourceTable.Layout_info_down,
                                new OnBottomPosCallback(80),
                                new OvalLightShape(5, 5, 20));
        mHightLight.show();
```

## 事件监听：

```java
		.setClickCallback(new HighLightInterface.OnClickCallback() {
            @Override
            public void onClick() {
                // 点击高亮布局的回调
            }
        })
        .setOnNextCallback(new HighLightInterface.OnNextCallback() {
            @Override
            public void onNext(HightLightView hightLightView, Component targetView, Component tipView) {
                // 高亮布局按次序加载布局的回调
            }
        })
        .setOnLayoutCallback(new HighLightInterface.OnLayoutCallback() {
            @Override
            public void onLayouted() {
                // 高亮布局加载完成的回调
            }
        })
        .setOnRemoveCallback(new HighLightInterface.OnRemoveCallback() {
            @Override
            public void onRemove() {
                // 高亮布局被移除的回调
            }
        })
        .setOnShowCallback(new HighLightInterface.OnShowCallback() {
            @Override
            public void onShow(HightLightView hightLightView) {
                // 高亮布局显示成功的回调
            }
        })
```
## 版本迭代

- v1.0.4

## 版权及许可信息

- Apache License



