package zhy.com.highlight.position;

import zhy.com.highlight.util.RectF;
import zhy.com.highlight.HighLight;

/**
 * Created by caizepeng on 16/8/20.
 */
public class OnBottomPosCallback extends OnBaseCallback {
    public OnBottomPosCallback() {}

    public OnBottomPosCallback(float offset) {
        super(offset);
    }

    /**
     * offset > 0  提示布局位置向下偏移，反之往上偏移
     *
     * @param bottomMargin 下间距
     * @param marginInfo   MarginInfo
     * @param rightMargin  右间距
     * @param rectF        RectF
     */
    @Override
    public void getPosition(float rightMargin, float bottomMargin, RectF rectF, HighLight.MarginInfo marginInfo) {
        marginInfo.rightMargin = rightMargin;
        marginInfo.topMargin = rectF.top + rectF.getHeight() + offset;
    }
}
