package zhy.com.highlight.position;

import zhy.com.highlight.util.RectF;
import zhy.com.highlight.HighLight;

/**
 * Created by caizepeng on 16/8/20.
 */
public class OnLeftPosCallback extends OnBaseCallback {
    public OnLeftPosCallback() {}

    public OnLeftPosCallback(float offset) {
        super(offset);
    }

    /**
     * offset > 0  提示布局位置向左偏移，反之往右偏移
     *
     * @param bottomMargin 下间距
     * @param marginInfo   MarginInfo
     * @param rightMargin  右间距
     * @param rectF        RectF
     */
    @Override
    public void getPosition(float rightMargin, float bottomMargin, RectF rectF, HighLight.MarginInfo marginInfo) {
        marginInfo.rightMargin = rightMargin + rectF.getWidth() + offset;
        marginInfo.topMargin = rectF.top;
    }
}
