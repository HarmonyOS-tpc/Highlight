/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zhy.com.highlight.util;

import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

/**
 * 控件范围构造工具类
 */
public class RectF extends RectFloat {
    public RectF(RectF rectF) {
        if (rectF == null) {
            left = top = right = bottom = 0.0f;
        } else {
            left = rectF.left;
            top = rectF.top;
            right = rectF.right;
            bottom = rectF.bottom;
        }
    }

    public RectF(Rect rect) {
        if (rect == null) {
            left = top = right = bottom = 0.0f;
        } else {
            left = rect.left;
            top = rect.top;
            right = rect.right;
            bottom = rect.bottom;
        }
    }

    /**
     * 添加偏移量
     *
     * @param dx X轴偏移量
     * @param dy Y轴偏移量
     */
    public void inset(float dx, float dy) {
        left += dx;
        top += dy;
        right -= dx;
        bottom -= dy;
    }

    @Override
    public String toString() {
        return "RectF{" + "bottom=" + bottom + ", left=" + left + ", right=" + right + ", top=" + top + '}';
    }
}
