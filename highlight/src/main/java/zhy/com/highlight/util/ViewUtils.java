package zhy.com.highlight.util;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Rect;
import ohos.app.Context;

import java.lang.reflect.Field;

/**
 * Created by zhy on 15/10/8.
 * 获取高亮view在其父容器中的坐标位置
 */
public class ViewUtils {
    private static final String FRAGMENT_CON = "NoSaveStateFrameLayout";

    public static Rect getLocationInView(Component parent, Component child) {
        if (child == null || parent == null) {
            throw new IllegalArgumentException("parent and child can not be null .");
        }
        Component decorView = null;
        Context context = child.getContext();
        if (context instanceof AbilitySlice) {
            decorView = getCurComponentContainer((AbilitySlice) parent.getContext());
        }

        Rect result = new Rect();
        Rect tmpRect = new Rect();

        Component tmp = child;

        if (child == parent) {
            result = child.getComponentPosition();
            return result;
        }
        while (tmp != decorView && tmp != parent) {
            tmpRect = tmp.getComponentPosition();
            if (!FRAGMENT_CON.equals(tmp.getClass().getSimpleName())) {
                result.left += tmpRect.left;
                result.top += tmpRect.top;
            }
            tmp = (Component) tmp.getComponentParent();
            // added by isanwenyu@163.com fix bug #21 the wrong rect user will received in ViewPager
            if (tmp != null && tmp.getComponentParent() != null && (tmp.getComponentParent() instanceof PageSlider)) {
                tmp = (Component) tmp.getComponentParent();
            }
        }
        result.right = result.left + child.getWidth();
        result.bottom = result.top + child.getHeight();
        return result;
    }

    /**
     * 获取AbilitySlice的根布局
     *
     * @param abilitySlice abilitySlice对象
     * @return 根布局
     */
    public static Component getCurComponentContainer(AbilitySlice abilitySlice) {
        try {
            Field uiContent = AbilitySlice.class.getDeclaredField("uiContent");
            uiContent.setAccessible(true);
            Object uiContentObj = uiContent.get(abilitySlice);

            Field curComponentContainer = uiContentObj.getClass().getSuperclass()
                    .getDeclaredField("curComponentContainer");
            curComponentContainer.setAccessible(true);
            Object curComponentContainerObj = curComponentContainer.get(uiContentObj);

            return (Component) curComponentContainerObj;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
