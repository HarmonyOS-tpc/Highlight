package zhy.com.highlight;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.PacMap;
import zhy.com.highlight.interfaces.HighLightInterface;
import zhy.com.highlight.shape.RectLightShape;
import zhy.com.highlight.util.LogUtil;
import zhy.com.highlight.util.RectF;
import zhy.com.highlight.util.ViewUtils;
import zhy.com.highlight.view.HightLightView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhy on 15/10/8.
 * 绘制的遮罩和提示框进行配置的类
 */
public class HighLight implements HighLightInterface, Component.LayoutRefreshedListener {
    private static final String TAG = "HighLight";
    private int mPosition = -1; // 当前显示的提示布局位置
    private HighLight.ViewPosInfo mViewPosInfo; // 当前显示的高亮布局位置信息

    public static class ViewPosInfo {
        public int layoutId = -1;
        public RectF rectF;
        public MarginInfo marginInfo;
        public Component view;
        public OnPosCallback onPosCallback;
        public LightShape lightShape;

        @Override
        public String toString() {
            return "ViewPosInfo{"
                    + "layoutId="
                    + layoutId
                    + ", rectF="
                    + rectF
                    + ", marginInfo="
                    + marginInfo
                    + ", view="
                    + view
                    + ", onPosCallback="
                    + onPosCallback
                    + ", lightShape="
                    + lightShape
                    + '}';
        }
    }

    public interface LightShape {
        public void shape(PixelMap bitmap, ViewPosInfo viewPosInfo);
    }

    public static class MarginInfo {
        public float topMargin;
        public float leftMargin;
        public float rightMargin;
        public float bottomMargin;
    }

    public static interface OnPosCallback {
        void getPos(float rightMargin, float bottomMargin, RectF rectF, MarginInfo marginInfo);
    }

    private Component mAnchor;
    private List<ViewPosInfo> mViewRects;
    private Context mContext;
    private HightLightView mHightLightView;
    private OnClickCallback clickCallback;

    private boolean intercept = true;
    // private boolean shadow = true;
    private int maskColor = 0xCC000000; // 浅灰色

    // added by isanwenyu@163.com
    private boolean autoRemove = true; // 点击是否自动移除 默认为true
    private boolean isNext = false; // next模式标志 默认为false
    private boolean mShowing; // 是否显示
    private InnerEvent mShowMessage;
    private InnerEvent mRemoveMessage;
    private InnerEvent mClickMessage;
    private InnerEvent mNextMessage;
    private InnerEvent mLayoutMessage;
    private ListenersHandler mListenersHandler;

    private static final int CLICK = 0x40;
    private static final int REMOVE = 0x41;
    private static final int SHOW = 0x42;
    private static final int NEXT = 0x43;
    private static final int LAYOUT = 0x44;

    public HighLight(Context context) {
        mContext = context;
        mViewRects = new ArrayList<>();
        mAnchor = ViewUtils.getCurComponentContainer((AbilitySlice) context);
        mListenersHandler = new ListenersHandler(EventRunner.getMainEventRunner(), this);
        registerGlobalLayoutListener();
    }

    public HighLight anchor(Component anchor) {
        mAnchor = anchor;
        registerGlobalLayoutListener();
        return this;
    }

    @Override
    public Component getAnchor() {
        return mAnchor;
    }

    public HighLight intercept(boolean intercept) {
        this.intercept = intercept;
        return this;
    }

    public HighLight maskColor(int maskColor) {
        this.maskColor = maskColor;
        return this;
    }

    public HighLight addHighLight(int viewId, int decorLayoutId, OnPosCallback onPosCallback, LightShape lightShape) {
        ComponentContainer parent = (ComponentContainer) mAnchor;
        Component view = parent.findComponentById(viewId);
        addHighLight(view, decorLayoutId, onPosCallback, lightShape);
        return this;
    }

    /**
     * 更新高亮布局
     */
    public void updateInfo() {
        ComponentContainer parent = (ComponentContainer) mAnchor;
        for (ViewPosInfo viewPosInfo : mViewRects) {
            RectF rect = new RectF(ViewUtils.getLocationInView(parent, viewPosInfo.view));
            {
                viewPosInfo.rectF = rect;
                viewPosInfo.onPosCallback.getPos(
                        parent.getWidth() - rect.right, parent.getHeight() - rect.bottom, rect, viewPosInfo.marginInfo);
            }
        }
    }

    /**
     * 添加一个高亮布局
     *
     * @param view          需要高亮的view
     * @param decorLayoutId 提示布局信息
     * @param onPosCallback 提示布局信息在view的上下左右显示的方位
     * @param lightShape    高亮view的形状：矩形，圆形，椭圆等，可自定义
     * @return HighLight
     */
    public HighLight addHighLight(
            Component view, int decorLayoutId, OnPosCallback onPosCallback, LightShape lightShape) {
        if (onPosCallback == null && decorLayoutId != -1) {
            throw new IllegalArgumentException("onPosCallback can not be null.");
        }
        ComponentContainer parent = (ComponentContainer) mAnchor;
        // 获取高亮布局在mAnchor中的位置
        RectF rect = null;
        if (view.getComponentParent() == mAnchor) {
            rect = new RectF(new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
        } else {
            rect = new RectF(ViewUtils.getLocationInView(parent, view));
        }
        // if RectF is empty return  added by isanwenyu 2016/10/26.
        if (rect.isEmpty()) {
            return this;
        }
        ViewPosInfo viewPosInfo = new ViewPosInfo();
        viewPosInfo.layoutId = decorLayoutId;
        viewPosInfo.rectF = rect;
        viewPosInfo.view = view;
        MarginInfo marginInfo = new MarginInfo();
        onPosCallback.getPos(parent.getWidth() - rect.right, parent.getHeight() - rect.bottom, rect, marginInfo);
        viewPosInfo.marginInfo = marginInfo;
        viewPosInfo.onPosCallback = onPosCallback;
        viewPosInfo.lightShape = lightShape == null ? new RectLightShape() : lightShape;
        mViewRects.add(viewPosInfo);
        return this;
    }

    // 一个场景可能有多个步骤的高亮。一个步骤完成之后再进行下一个步骤的高亮
    // 添加点击事件，将每次点击传给应用逻辑
    public HighLight setClickCallback(OnClickCallback clickCallback) {
        if (clickCallback != null) {
            mClickMessage = InnerEvent.get(CLICK, clickCallback);
        } else {
            mClickMessage = null;
        }
        return this;
    }

    public HighLight setOnShowCallback(OnShowCallback onShowCallback) {
        if (onShowCallback != null) {
            mShowMessage = InnerEvent.get(SHOW, onShowCallback);

        } else {
            mShowMessage = null;
        }
        return this;
    }

    public HighLight setOnRemoveCallback(OnRemoveCallback onRemoveCallback) {
        if (onRemoveCallback != null) {
            mRemoveMessage = InnerEvent.get(REMOVE, onRemoveCallback);

        } else {
            mRemoveMessage = null;
        }
        return this;
    }

    /**
     * 监听下一步动作
     *
     * @param onNextCallback 监听下一步动作
     * @return HighLight
     */
    public HighLight setOnNextCallback(OnNextCallback onNextCallback) {
        if (onNextCallback != null) {
            mNextMessage = InnerEvent.get(NEXT, onNextCallback);
        } else {
            mNextMessage = null;
        }
        return this;
    }

    /**
     * 设置根布局mAnchor全局布局监听器
     *
     * @param onLayoutCallback 布局完成后的监听器
     * @return HighLight
     * @see #registerGlobalLayoutListener()
     */
    public HighLight setOnLayoutCallback(OnLayoutCallback onLayoutCallback) {
        if (onLayoutCallback != null) {
            mLayoutMessage = InnerEvent.get(LAYOUT, onLayoutCallback);
        } else {
            mLayoutMessage = null;
        }
        return this;
    }

    /**
     * 高亮布局是否显示中
     *
     * @return Whether the dialog is currently showing.
     * @author isanwenyu@163.com
     */
    public boolean isShowing() {
        return mShowing;
    }

    /**
     * 点击后是否自动移除
     *
     * @param autoRemove 自动移除高亮布局
     * @return 链式接口 返回自身
     * @author isanwenyu@163.com
     * @see #show()
     * @see #remove()
     */
    public HighLight autoRemove(boolean autoRemove) {
        this.autoRemove = autoRemove;
        return this;
    }

    /**
     * 获取高亮布局 如果要获取decorLayout中布局请在{@link #show()}后调用
     * <p>
     * 高亮布局的id在{@link #show()}中hightLightView.setId(R.id.high_light_view)设置
     *
     * @return 返回id为R.id.high_light_view的高亮布局对象
     * @author isanwenyu@163.com
     * @see #show()
     */
    @Override
    public HightLightView getHightLightView() {
        if (mHightLightView != null) {
            LogUtil.info(" debug getHightLightView", "mHightLightView  not null");
            return mHightLightView;
        }
        mHightLightView = (HightLightView) ((AbilitySlice) mContext).findComponentById(1001);
        LogUtil.info(" debug getHightLightView", "mHightLightView  " + mHightLightView);
        return mHightLightView;
    }

    /**
     * 开启next模式
     *
     * @return 链式接口 返回自身
     * @author isanwenyu@163.com
     * @see #show()
     */
    public HighLight enableNext() {
        this.isNext = true;
        return this;
    }

    /**
     * 返回是否是next模式
     *
     * @return 是否显示下一个高亮布局
     * @author isanwenyu@163.com
     */
    public boolean isNext() {
        return isNext;
    }

    /**
     * 切换到下个提示布局
     *
     * @return HighLight自身对象
     * @author isanwenyu@163.com
     */
    @Override
    public HighLight next() {
        if (getHightLightView() == null) {
            throw new NullPointerException("The HightLightView is null,you must invoke show() " + "before this!");
        } else {
            addViewForTip((ComponentContainer) getHightLightView().getComponentParent());
        }
        return this;
    }

    @Override
    public HighLight show() {
        LogUtil.info("debug", "start   show intercept:" + intercept + "  autoRemove: " + autoRemove);
        if (getHightLightView() != null) {
            mHightLightView = getHightLightView();
            // 重置当前HighLight对象属性
            mShowing = true;
            isNext = mHightLightView.isNext();
            // TODO: 2016/11/16 按需重置其他属性
            return this;
        }
        // 如果View rect 容器为空 直接返回 added by isanwenyu 2016/10/26.
        if (mViewRects.isEmpty()) {
            LogUtil.warn("debug", " show  mViewRects.isEmpty");
            return this;
        }
        HightLightView hightLightView = new HightLightView(mContext, this, maskColor, mViewRects, isNext);
        // add high light view unique id by isanwenyu@163.com  on 2016/9/28.
        hightLightView.setId(1001);
        mHightLightView = hightLightView;
        if (intercept) {
            mAnchor.setEnabled(false);
        }
        if (mAnchor instanceof StackLayout) {
            StackLayout.LayoutConfig lp =
                    new StackLayout.LayoutConfig(
                            ComponentContainer.LayoutConfig.MATCH_PARENT, (StackLayout.LayoutConfig.MATCH_PARENT));
            ((StackLayout) mAnchor).addComponent(hightLightView, ((StackLayout) mAnchor).getChildCount(), lp);
            addViewForTip((ComponentContainer) mAnchor);
        } else {
            StackLayout frameLayout = new StackLayout(mContext);
            ComponentContainer parent = (ComponentContainer) mAnchor.getComponentParent();
            parent.removeComponent(mAnchor);
            parent.addComponent(frameLayout, mAnchor.getLayoutConfig());
            StackLayout.LayoutConfig lp =
                    new StackLayout.LayoutConfig(
                            StackLayout.LayoutConfig.MATCH_PARENT, StackLayout.LayoutConfig.MATCH_PARENT);
            frameLayout.addComponent(mAnchor, lp);
            frameLayout.addComponent(hightLightView, lp);
            addViewForTip(frameLayout);
            parent.invalidate();
        }

        if (intercept) {
            hightLightView.setTouchEventListener(new Component.TouchEventListener() {
                @Override
                public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                    if (TouchEvent.PRIMARY_POINT_UP == touchEvent.getAction()) {
                        if (autoRemove) {
                            remove();
                        }
                        sendClickMessage();
                    }
                    return true;
                }
            });
        }
        LogUtil.info("debug", " show   start  addViewForTip()  ");
        // 延迟添加提示布局
        //hightLightView.addViewForTip();
        mShowing = true;
        // 响应显示回调
        sendShowMessage();
        return this;
    }

    @Override
    public HighLight remove() {
        if (getHightLightView() == null) {
            LogUtil.info("debug", " remove  getHightLightView() == null");
            return this;
        }
        ComponentContainer parent = (ComponentContainer) mHightLightView.getComponentParent();

        if (parent instanceof DependentLayout || parent instanceof StackLayout) {
            removeAllTips(parent);
            parent.removeComponent(mHightLightView);
            parent.invalidate();
        } else {
            removeAllTips(parent);
            parent.removeComponent(mHightLightView);
            Component origin = parent.getComponentAt(0);
            ComponentContainer graParent = (ComponentContainer) parent.getComponentParent();
            graParent.removeComponent(parent);
            graParent.invalidate();
            graParent.addComponent(origin, parent.getLayoutConfig());
        }
        mHightLightView = null;
        sendRemoveMessage();
        mShowing = false;
        mAnchor.setEnabled(true);
        return this;
    }

    private void sendClickMessage() {
        if (mClickMessage != null) {
            // Obtain a new message so this highlight can be re-used
            try {
                mListenersHandler.sendEvent(InnerEvent.get(mClickMessage));
            } catch (CloneNotSupportedException e) {
                LogUtil.error("sendClickMessage", "CloneNotSupportedException");
            }
        }
    }

    private void sendRemoveMessage() {
        if (mRemoveMessage != null) {
            try {
                // Obtain a new message so this highlight can be re-used
                mListenersHandler.sendEvent(InnerEvent.get(mRemoveMessage));
            } catch (CloneNotSupportedException e) {
                LogUtil.error("sendRemoveMessage", "CloneNotSupportedException");
            }
        }
    }

    private void sendShowMessage() {
        if (mShowMessage != null) {
            // Obtain a new message so this highlight can be re-used
            try {
                mListenersHandler.sendEvent(InnerEvent.get(mShowMessage));
            } catch (CloneNotSupportedException e) {
                LogUtil.error("sendShowMessage", "CloneNotSupportedException");
            }
        }
    }

    private void sendLayoutMessage() {
        if (mLayoutMessage != null) {
            // Obtain a new message so this highlight can be re-used
            try {
                mListenersHandler.sendEvent(InnerEvent.get(mLayoutMessage));
            } catch (CloneNotSupportedException e) {
                LogUtil.error("sendLayoutMessage", "CloneNotSupportedException");
            }
        }
    }

    public void sendNextMessage() {
        if (!isNext) {
            throw new IllegalArgumentException("only for isNext mode,please invoke enableNext() " + "first");
        }

        if (getHightLightView() == null) {
            LogUtil.warn("debug sendNextMessage", "getHightLightView == null");
            return;
        }
        // 发送下一步消息事件
        ViewPosInfo viewPosInfo = mViewPosInfo;
        if (mNextMessage != null && viewPosInfo != null) {
            PacMap pacMap = new PacMap();
            pacMap.putObjectValue("arg1", viewPosInfo.view == null ? -1 : viewPosInfo.view.getId());
            pacMap.putObjectValue("arg2", viewPosInfo.layoutId);
            mNextMessage.setPacMap(pacMap);
            try {
                mListenersHandler.sendEvent(InnerEvent.get(mNextMessage));
            } catch (CloneNotSupportedException e) {
                LogUtil.error("sendNextMessage", "CloneNotSupportedException");
            }
        }
    }

    /**
     * 为mAnchor注册全局布局监听器
     */
    private void registerGlobalLayoutListener() {
        mAnchor.setLayoutRefreshedListener(this);
    }

    /**
     * mAnchor反注册全局布局监听器
     */
    private void unRegisterGlobalLayoutListener() {
        mAnchor.setLayoutRefreshedListener(null);
    }

    @Override
    public void onRefreshed(Component component) {
        unRegisterGlobalLayoutListener();
        sendLayoutMessage();
    }

    /**
     * @see ListenersHandler
     */
    private static final class ListenersHandler extends EventHandler {
        private WeakReference<HighLightInterface> mHighLightInterface;
        private HightLightView hightLightView;
        private Component anchorView;

        public ListenersHandler(EventRunner runner, HighLight highLight) {
            super(runner);
            mHighLightInterface = new WeakReference<>(highLight);
        }

        @Override
        public void processEvent(InnerEvent msg) {
            super.processEvent(msg);
            hightLightView = mHighLightInterface.get() == null ? null : mHighLightInterface.get().getHightLightView();
            anchorView = mHighLightInterface.get() == null ? null : mHighLightInterface.get().getAnchor();
            switch (msg.eventId) {
                case CLICK:
                    ((OnClickCallback) msg.object).onClick();
                    break;
                case REMOVE:
                    ((OnRemoveCallback) msg.object).onRemove();
                    break;
                case SHOW:
                    ((OnShowCallback) msg.object).onShow(hightLightView);
                    break;
                case NEXT:
                    LogUtil.warn("debug processEvent", "NEXT ");
                    PacMap pacMap = msg.getPacMap();
                    Component targetView =
                            anchorView != null
                                    ? anchorView.findComponentById((Integer) pacMap.getObjectValue("arg1").get())
                                    : null;
                    Component tipView =
                            hightLightView != null
                                    ? hightLightView.findComponentById((Integer) pacMap.getObjectValue("arg2").get())
                                    : null;
                    ((OnNextCallback) msg.object).onNext(hightLightView, targetView, tipView);
                    break;
                case LAYOUT:
                    ((OnLayoutCallback) msg.object).onLayouted();
                    break;
            }
        }
    }

    /**
     * 添加引导布局
     *
     * @param parent 引导布局的父布局
     */
    public void addViewForTip(ComponentContainer parent) {
        LogUtil.info("debug", "addViewForTip isNext:" + isNext + "  mPosition:" + mPosition);
        if (isNext) {
            // 校验mPosition
            if (mPosition < -1 || mPosition > mViewRects.size() - 1) {
                // 重置位置
                mPosition = 0;
            } else if (mPosition == mViewRects.size() - 1) {
                // 移除当前布局
                remove();
                return;
            } else {
                // mPosition++
                mPosition++;
            }
            mViewPosInfo = mViewRects.get(mPosition);
            // 移除所有tip再添加当前位置的tip布局
            removeAllTips(parent);
            addViewForEveryTip(parent, mViewPosInfo);
            // 更新高亮布局中高亮空间位置信息
            getHightLightView().setCurrentViewPosInfo(mViewPosInfo);
            LogUtil.info("debug", "addViewForTip isNext:" + isNext);
            sendNextMessage();
        } else {
            for (HighLight.ViewPosInfo viewPosInfo : mViewRects) {
                addViewForEveryTip(parent, viewPosInfo);
            }
        }
    }

    /**
     * 添加每个高亮布局
     *
     * @param container   引导布局父布局
     * @param viewPosInfo 高亮布局信息
     * @author isanwenyu@163.com
     */
    private void addViewForEveryTip(ComponentContainer container, HighLight.ViewPosInfo viewPosInfo) {
        Component view = LayoutScatter.getInstance(mContext).parse(viewPosInfo.layoutId, container, false);
        // 设置id为layout id 供HighLight查找
        view.setId(viewPosInfo.layoutId);
        StackLayout.LayoutConfig lp = buildTipLayoutParams(view, viewPosInfo);

        if (lp == null) {
            return;
        }

        lp.setMargins(
                (int) viewPosInfo.marginInfo.leftMargin,
                (int) viewPosInfo.marginInfo.topMargin,
                (int) viewPosInfo.marginInfo.rightMargin,
                (int) viewPosInfo.marginInfo.bottomMargin);

        if (lp.getMarginRight() != 0) {
            lp.alignment = LayoutAlignment.RIGHT;
        } else {
            lp.alignment = LayoutAlignment.LEFT;
        }

        if (lp.getMarginBottom() != 0) {
            lp.alignment |= LayoutAlignment.BOTTOM;
        } else {
            lp.alignment |= LayoutAlignment.TOP;
        }
        container.addComponent(view, lp);
    }

    /**
     * 提示布局的LayoutParams更新
     *
     * @param view        view
     * @param viewPosInfo view位置信息
     * @return LayoutConfig
     */
    private StackLayout.LayoutConfig buildTipLayoutParams(Component view, HighLight.ViewPosInfo viewPosInfo) {
        StackLayout.LayoutConfig lp = (StackLayout.LayoutConfig) view.getLayoutConfig();
        if (lp.getMarginLeft() == (int) viewPosInfo.marginInfo.leftMargin
                && lp.getMarginTop() == (int) viewPosInfo.marginInfo.topMargin
                && lp.getMarginRight() == (int) viewPosInfo.marginInfo.rightMargin
                && lp.getMarginBottom() == (int) viewPosInfo.marginInfo.bottomMargin) {
            return null;
        }
        lp.setMargins(
                (int) viewPosInfo.marginInfo.leftMargin,
                (int) viewPosInfo.marginInfo.topMargin,
                (int) viewPosInfo.marginInfo.rightMargin,
                (int) viewPosInfo.marginInfo.bottomMargin);

        if (lp.getMarginRight() != 0) {
            lp.alignment = LayoutAlignment.RIGHT;
        } else {
            lp.alignment = LayoutAlignment.LEFT;
        }

        if (lp.getMarginBottom() != 0) {
            lp.alignment |= LayoutAlignment.BOTTOM;
        } else {
            lp.alignment |= LayoutAlignment.TOP;
        }
        return lp;
    }

    /**
     * 移除当前高亮布局的所有提示布局
     * ohos中移除组件后还需要调用invalidate重绘
     *
     * @param parent 引导布局父布局
     */
    private void removeAllTips(ComponentContainer parent) {
        if (isNext) {
            if (null == mViewPosInfo) {
                return;
            }
            parent.removeComponentById(mViewPosInfo.layoutId);
        } else {
            if (null == mViewRects || 0 == mViewRects.size()) {
                return;
            }
            for (ViewPosInfo viewPosInfo : mViewRects) {
                parent.removeComponentById(viewPosInfo.layoutId);
            }
        }
    }
}
