package zhy.com.highlight.view;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.Canvas.PorterDuffMode;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import zhy.com.highlight.HighLight;
import zhy.com.highlight.util.LogUtil;

import java.util.List;

/**
 * Created by zhy on 15/10/8.
 */
public class HightLightView extends StackLayout implements Component.EstimateSizeListener,
        ComponentContainer.ArrangeListener, Component.BindStateChangedListener {
    private PixelMap mMaskBitmap;
    private PixelMap mLightBitmap;
    private PixelMap mTransparentBitmap; // 透明背景层
    private Paint mPaint;
    private Paint mNormalPaint;
    private List<HighLight.ViewPosInfo> mViewRects;
    private HighLight mHighLight;
    private LayoutScatter mInflater;

    // private boolean isBlur = true;
    private int maskColor = 0xCCFF6600;

    // added by isanwenyu@163.com
    private final boolean isNext; // next模式标志
    private HighLight.ViewPosInfo mViewPosInfo; // 当前显示的高亮布局位置信息

    private int mLeft = 0;
    private int mWidth = 0;
    private int mTop = 0;
    private int mHeight = 0;
    private boolean isLayoutChanged = false; // 是否重新布局

    public HightLightView(
            Context context,
            HighLight highLight,
            int maskColor,
            List<HighLight.ViewPosInfo> viewRects,
            boolean isNext) {
        super(context);
        mHighLight = highLight;
        mInflater = LayoutScatter.getInstance(context);
        mViewRects = viewRects;
        this.maskColor = maskColor;
        this.isNext = isNext;

        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);

        mNormalPaint = new Paint();
        mNormalPaint.setDither(true);
        mNormalPaint.setAntiAlias(true);
        mNormalPaint.setStyle(Paint.Style.FILL_STYLE);

        addDrawTask(
                new DrawTask() {
                    @Override
                    public void onDraw(Component component, Canvas canvas) {
                        try {
                            // 控制蒙版的背景显示
                            canvas.drawPixelMapHolder(new PixelMapHolder(mTransparentBitmap), 0, 0, mNormalPaint);
                        } catch (Exception e) {
                            LogUtil.error("onDraw", "Exception");
                        }
                    }
                });
        setEstimateSizeListener(this);
        setArrangeListener(this);
        setBindStateChangedListener(this);
    }

    private void buildMask() {
        recycleBitmap(mTransparentBitmap);
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(getWidth(), getHeight());
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;

        // 实现透明背景层
        mTransparentBitmap = PixelMap.create(initializationOptions);
        Canvas bgTransparentCanvas = new Canvas(new Texture(mTransparentBitmap));
        bgTransparentCanvas.drawColor(new Color(0x00000000).getValue(), PorterDuffMode.SRC_OVER);

        recycleBitmap(mMaskBitmap);

        //        mMaskBitmap = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_4444);
        //        anddroid API级别29中已弃用该字段ARGB_4444。由于此配置的质量较差，建议改为使用ARGB_8888。
        mMaskBitmap = PixelMap.create(initializationOptions);
        //        Canvas canvas = new Canvas(mMaskBitmap);
        Canvas canvas = new Canvas(new Texture(mMaskBitmap));
        //        canvas.drawColor(maskColor);
        canvas.drawRect(0, 0, getWidth(), getHeight(), mNormalPaint, new Color(maskColor)); // 绘制蒙版
        bgTransparentCanvas.drawPixelMapHolder(new PixelMapHolder(mMaskBitmap), 0, 0, mNormalPaint);

        //        mPaint.setXfermode(MODE_DST_OUT);设置混合模式
        mPaint.setBlendMode(BlendMode.DST_OUT);
        mHighLight.updateInfo();

        recycleBitmap(mLightBitmap);

        mLightBitmap = PixelMap.create(initializationOptions);
        if (isNext) { // 如果是next模式添加每个提示布局的背景形状
            // 添加当前提示布局的高亮形状背景
            addViewEveryTipShape(mViewPosInfo);
        } else {
            for (HighLight.ViewPosInfo viewPosInfo : mViewRects) {
                addViewEveryTipShape(viewPosInfo);
            }
        }
        //        canvas.drawBitmap(mLightBitmap, 0, 0, mPaint);
        bgTransparentCanvas.drawPixelMapHolder(new PixelMapHolder(mLightBitmap), 0, 0, mPaint);
    }

    /**
     * 主动回收之前创建的bitmap
     *
     * @param bitmap
     */
    private void recycleBitmap(PixelMap bitmap) {
        if (bitmap != null && !bitmap.isReleased()) {
            bitmap.release();
            bitmap = null;
        }
    }

    /**
     * 添加提示布局的背景形状
     *
     * @param viewPosInfo //提示布局的位置信息
     * @author isanwenyu@16.com
     */
    private void addViewEveryTipShape(HighLight.ViewPosInfo viewPosInfo) {
        viewPosInfo.lightShape.shape(mLightBitmap, viewPosInfo);
    }


    public boolean isNext() {
        return isNext;
    }

    /**
     * 获取当前高亮提示布局信息
     *
     * @return 当前高亮提示布局信息
     */
    public HighLight.ViewPosInfo getCurentViewPosInfo() {
        return mViewPosInfo;
    }

    /**
     * 测量
     *
     * @param widthMeasureSpec  布局宽度相关参数
     * @param heightMeasureSpec 布局高度相关参数
     * @return 是否完成测量
     */
    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int width = EstimateSpec.getSize(widthMeasureSpec);
        int height = EstimateSpec.getSize(heightMeasureSpec);
        LogUtil.info("onEstimateSize", "component width: " + width + " height: " + height);
        setEstimatedSize(width, height);
        return false;
    }

    /**
     * 布局
     *
     * @param left   布局左上角点的left参数
     * @param top    布局左上角点的top参数
     * @param width  布局宽度
     * @param height 布局高度
     * @return 是否完成布局
     */
    @Override
    public boolean onArrange(int left, int top, int width, int height) {
        if (left != mLeft || top != mTop || width != mWidth || height != mHeight) {
            isLayoutChanged = true;
            mLeft = left;
            mWidth = width;
            mTop = top;
            mHeight = height;
        } else {
            isLayoutChanged = false;
        }
        if (isLayoutChanged || isNext) {
            LogUtil.info("onArrange", "component width: " + width + " height: " + height);
            buildMask();
        }
        return false;
    }

    /**
     * Next 模式下，更新引导布局位置
     *
     * @param viewPosInfo 新引导布局位置信息
     */
    public void setCurrentViewPosInfo(HighLight.ViewPosInfo viewPosInfo) {
        this.mViewPosInfo = viewPosInfo;
        postLayout();
        invalidate();
    }

    @Override
    public void onComponentBoundToWindow(Component component) {

    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        recycleBitmap(mLightBitmap);
        recycleBitmap(mMaskBitmap);
        recycleBitmap(mTransparentBitmap);
    }
}
