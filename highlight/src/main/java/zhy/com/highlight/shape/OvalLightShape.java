package zhy.com.highlight.shape;

import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;
import zhy.com.highlight.util.RectF;
import zhy.com.highlight.HighLight;

/**
 * 椭圆形高亮形状
 * Created by isanwenyu on 16/11/15.
 * Edited by isanwenyu@163.com 16/10/26.
 */
public class OvalLightShape extends BaseLightShape {
    public OvalLightShape() {
        super();
    }

    public OvalLightShape(float dx, float dy) {
        super(dx, dy);
    }

    public OvalLightShape(float dx, float dy, float blurRadius) {
        super(dx, dy, blurRadius);
    }

    @Override
    protected void drawShape(PixelMap bitmap, HighLight.ViewPosInfo viewPosInfo) {
        Canvas canvas = new Canvas(new Texture(bitmap));
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setAntiAlias(true);
        if (blurRadius > 0) {
            paint.setMaskFilter(new MaskFilter(blurRadius, MaskFilter.Blur.SOLID));
        }
        RectFloat rectF = viewPosInfo.rectF;
        canvas.drawOval(rectF, paint);
    }

    @Override
    protected void resetRectF4Shape(RectF viewPosInfoRectF, float dx, float dy) {
        // 默认根据dx dy横向和竖向缩小RectF范围
        viewPosInfoRectF.inset(dx, dy);
    }
}
