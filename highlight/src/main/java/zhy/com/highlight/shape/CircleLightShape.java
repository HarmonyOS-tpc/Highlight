package zhy.com.highlight.shape;

import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;
import zhy.com.highlight.util.RectF;
import zhy.com.highlight.HighLight;

/**
 * Created by caizepeng on 16/8/20.
 * Edited by isanwenyu@163.com 16/10/26.
 */
public class CircleLightShape extends BaseLightShape {
    public CircleLightShape() {
        super();
    }

    public CircleLightShape(float dx, float dy) {
        super(dx, dy);
    }

    public CircleLightShape(float dx, float dy, float blurRadius) {
        super(dx, dy, blurRadius);
    }

    @Override
    protected void drawShape(PixelMap bitmap, HighLight.ViewPosInfo viewPosInfo) {
        Canvas canvas = new Canvas(new Texture(bitmap));
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setAntiAlias(true);
        if (blurRadius > 0) {
            paint.setMaskFilter(new MaskFilter(blurRadius,MaskFilter.Blur.SOLID));
        }
        RectFloat rectF = viewPosInfo.rectF;
        canvas.drawCircle(rectF.left+(rectF.getWidth()/2),rectF.top+(rectF.getHeight()/2),
                Math.max(rectF.getWidth(),rectF.getHeight())/2,paint);
    }

    @Override
    protected void resetRectF4Shape(RectF viewPosInfoRectF, float dx, float dy) {
        viewPosInfoRectF.inset(dx,dy);
    }
}
