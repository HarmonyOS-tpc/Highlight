/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhy.highlight;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.Optional;

/**
 * ListContainer适配器
 */
public class ListContainerProvider extends BaseItemProvider {
    private String[] data;
    private Context mContext;

    /**
     * 构造器
     *
     * @param context 上下文
     * @param data    数据源
     */
    public ListContainerProvider(Context context, String[] data) {
        this.mContext = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return Optional.empty();
    }

    @Override
    public long getItemId(int position) {
        long id = position;
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        int index = position;
        Component contentView  = component;
        ComponentContainer container = componentContainer;
        ViewHolder viewHolder = null;
        if (contentView == null) {
            contentView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_list, container, false);
            viewHolder = new ViewHolder(contentView);
            contentView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) contentView.getTag();
        }

        viewHolder.text.setText(data[position] + "");
        return contentView;
    }

    /**
     * 子item初始化
     */
    public static class ViewHolder {
        Text text;

        public ViewHolder(Component itemView) {
            text = (Text) itemView.findComponentById(ResourceTable.Id_tv_text);
        }
    }
}
