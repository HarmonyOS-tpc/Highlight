/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhy.highlight.slice;

import com.zhy.highlight.ListContainerProvider;
import com.zhy.highlight.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.media.image.PixelMap;
import zhy.com.highlight.HighLight;
import zhy.com.highlight.interfaces.HighLightInterface;
import zhy.com.highlight.position.OnBottomPosCallback;
import zhy.com.highlight.position.OnLeftPosCallback;
import zhy.com.highlight.position.OnRightPosCallback;
import zhy.com.highlight.position.OnTopPosCallback;
import zhy.com.highlight.shape.BaseLightShape;
import zhy.com.highlight.shape.CircleLightShape;
import zhy.com.highlight.shape.OvalLightShape;
import zhy.com.highlight.shape.RectLightShape;
import zhy.com.highlight.util.RectF;
import zhy.com.highlight.view.HightLightView;

/**
 * Demo 程序逻辑类
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private HighLight mHightLight;
    private Button btn_show_known_tip;
    private Button btn_show_tip;
    private Button btn_show_next_known_tip;
    private Button btn_show_next_tip;
    private Button btn_light_1;
    private Button btn_light_2;
    private Button btn_light_3;
    private Component rootLayout;
    private ListContainer listContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        rootLayout = findComponentById(ResourceTable.Id_id_container);

        btn_show_known_tip = (Button) findComponentById(ResourceTable.Id_btn_show_known_tip);
        btn_show_known_tip.requestFocus();
        btn_show_known_tip.setClickedListener(this);

        btn_show_tip = (Button) findComponentById(ResourceTable.Id_btn_show_tip);
        btn_show_tip.requestFocus();
        btn_show_tip.setClickedListener(this);

        btn_show_next_known_tip = (Button) findComponentById(ResourceTable.Id_btn_show_next_known_tip);
        btn_show_next_known_tip.requestFocus();
        btn_show_next_known_tip.setClickedListener(this);

        btn_show_next_tip = (Button) findComponentById(ResourceTable.Id_btn_show_next_tip);
        btn_show_next_tip.requestFocus();
        btn_show_next_tip.setClickedListener(this);

        btn_light_1 = (Button) findComponentById(ResourceTable.Id_btn_light);
        btn_light_1.requestFocus();
        btn_light_1.setClickedListener(this);

        btn_light_2 = (Button) findComponentById(ResourceTable.Id_btn_right_light);
        btn_light_2.requestFocus();
        btn_light_2.setClickedListener(this);

        btn_light_3 = (Button) findComponentById(ResourceTable.Id_btn_bottom_light);
        btn_light_3.requestFocus();
        btn_light_3.setClickedListener(this);

        listContainer = (ListContainer) findComponentById(ResourceTable.Id_lc_list);

        // 初始化ListContainer相关数据
        initListContainer();

        showNextTipViewOnCreated();
    }

    /**
     * 当界面布局完成显示next模式提示布局
     * 显示方法必须在onLayouted中调用
     * 适用于Activity及Fragment中使用
     * 可以直接在onCreated方法中调用
     *
     * @author isanwenyu@163.com
     */
    public void showNextTipViewOnCreated() {
        mHightLight = new HighLight(MainAbilitySlice.this)
                .anchor(findComponentById(ResourceTable.Id_id_container)) // 如果是AbilitySlice上增加引导层，不需要设置anchor
                .autoRemove(false)
                .enableNext()
                .setOnLayoutCallback(new HighLightInterface.OnLayoutCallback() {
                    @Override
                    public void onLayouted() {
                        // 界面布局完成添加tipview
                        mHightLight.addHighLight(ResourceTable.Id_btn_right_light,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnLeftPosCallback(45),
                                new RectLightShape())
                                .addHighLight(ResourceTable.Id_btn_light,
                                        ResourceTable.Layout_info_gravity_left_down,
                                        new OnRightPosCallback(5),
                                        new CircleLightShape())
                                .addHighLight(ResourceTable.Id_btn_bottom_light,
                                        ResourceTable.Layout_info_gravity_left_down,
                                        new OnTopPosCallback(),
                                        new CircleLightShape());
                        // 然后显示高亮布局
                        mHightLight.show();
                    }
                })
                .setClickCallback(new HighLight.OnClickCallback() {
                    @Override
                    public void onClick() {
                        mHightLight.next();
                    }
                });
    }

    /**
     * 初始化列表
     */
    private void initListContainer() {
        String[] listData = getStringArray(ResourceTable.Strarray_entities);
        listContainer.setItemProvider(new ListContainerProvider(getContext(), listData));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    /**
     * 响应所有ResourceTable.ID_iv_known的控件的点击事件
     * 目前xml没有对应的onClick配置,等待后续支持后再处理
     * <p>
     * 移除高亮布局：点击该控件以外的区域不响应操作
     * </p>
     */
    public void clickKnown() {
        HightLightView decorLayout = mHightLight.getHightLightView();
        Image knownView = (Image) decorLayout.findComponentById(ResourceTable.Id_iv_known);
        knownView.requestFocus();
        knownView.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (mHightLight.isShowing() && mHightLight.isNext()) {
                            mHightLight.next();
                        } else {
                            remove();
                        }
                    }
                });
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_show_known_tip:
                showKnownTipView(btn_show_known_tip);
                break;
            case ResourceTable.Id_btn_show_tip:
                showTipView(btn_show_tip);
                break;
            case ResourceTable.Id_btn_show_next_known_tip:
                showNextKnownTipView(btn_show_next_known_tip);
                break;
            case ResourceTable.Id_btn_show_next_tip:
                showNextTipView(btn_show_next_tip);
                break;
        }
    }

    /**
     * 移除高亮展示布局
     */
    public void remove() {
        mHightLight.remove();
    }

    /**
     * 展示带有知道了按钮的高亮布局
     *
     * @param view 被点击的按钮
     */
    public void showKnownTipView(Component view) {
        mHightLight =
                new HighLight(MainAbilitySlice.this)
                        .autoRemove(false) // 设置背景点击高亮布局自动移除为false 默认为true
                        .intercept(true) // 设置拦截属性为false 高亮布局不影响后面布局的滑动效果 而且使下方点击回调失效
                        .setClickCallback(
                                new HighLight.OnClickCallback() {
                                    @Override
                                    public void onClick() {
                                        remove();
                                    }
                                })
                        // .anchor(rootLayout) // 不必须设置根布局，自动获取页面根布局
                        .maskColor(0xCC003300) // 设置蒙版背景色
                        // 在高亮按钮2 右边显示高亮提示布局
                        .addHighLight(
                                ResourceTable.Id_btn_right_light,
                                ResourceTable.Layout_info_known,
                                new OnLeftPosCallback(45),
                                new RectLightShape())
                        // 在高亮按钮1 左边显示高亮提示布局
                        .addHighLight(
                                ResourceTable.Id_btn_light,
                                ResourceTable.Layout_info_known,
                                new OnRightPosCallback(5),
                                new CircleLightShape(0, 0, 0))
                        // 在高亮按钮3 头顶显示高亮提示布局
                        .addHighLight(
                                ResourceTable.Id_btn_bottom_light,
                                ResourceTable.Layout_info_known,
                                new OnTopPosCallback(),
                                new CircleLightShape())
                        // 在showKnownTip 底部显示高亮提示布局
                        .addHighLight(
                                view,
                                ResourceTable.Layout_info_known,
                                new OnBottomPosCallback(10),
                                new OvalLightShape(5, 5, 20));
        mHightLight.show();
    }

    /**
     * 一次性展示所有引导布局的高亮布局
     *
     * @param view 被点击的按钮
     */
    public void showTipView(Component view) {
        mHightLight =
                new HighLight(MainAbilitySlice.this)
                        .anchor(rootLayout)
                        .addHighLight(
                                ResourceTable.Id_btn_right_light,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnLeftPosCallback(45),
                                new RectLightShape())
                        .addHighLight(
                                ResourceTable.Id_btn_light,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnRightPosCallback(5),
                                new CircleLightShape())
                        .addHighLight(
                                ResourceTable.Id_btn_bottom_light,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnTopPosCallback(),
                                new CircleLightShape())
                        .addHighLight(
                                view,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnBottomPosCallback(60),
                                new CircleLightShape());
        mHightLight.show();
    }

    /**
     * 按次序展示引导布局的高亮布局
     *
     * @param view 被点击的按钮
     */
    public void showNextKnownTipView(Component view) {
        mHightLight =
                new HighLight(MainAbilitySlice.this)
                        .anchor(rootLayout)
                        .autoRemove(false) // 设置背景点击高亮布局自动移除为false 默认为true
                        .intercept(true) // 拦截属性默认为true 使下方ClickCallback生效
                        .enableNext() // 开启next模式并通过show方法显示 然后通过调用next()方法切换到下一个提示布局，直到移除自身
                        .addHighLight(
                                ResourceTable.Id_btn_right_light,
                                ResourceTable.Layout_info_known,
                                new OnLeftPosCallback(45),
                                new RectLightShape(0, 0, 15, 0, 0)) // 矩形去除圆角
                        .addHighLight(
                                ResourceTable.Id_btn_light,
                                ResourceTable.Layout_info_known,
                                new OnRightPosCallback(5),
                                new BaseLightShape(5, 5, 0) {
                                    @Override
                                    protected void resetRectF4Shape(RectF viewPosInfoRectF, float dx, float dy) {
                                        // 缩小高亮控件范围
                                        viewPosInfoRectF.inset(dx, dy);
                                    }

                                    @Override
                                    protected void drawShape(PixelMap bitmap, HighLight.ViewPosInfo viewPosInfo) {
                                        // custom your hight light shape 自定义高亮形状
                                        Canvas canvas = new Canvas(new Texture(bitmap));
                                        Paint paint = new Paint();
                                        paint.setDither(true);
                                        paint.setAntiAlias(true);
                                        // blurRadius必须大于0
                                        if (blurRadius > 0) {
                                            paint.setMaskFilter(new MaskFilter(blurRadius, MaskFilter.Blur.SOLID));
                                        }
                                        RectF rectF = viewPosInfo.rectF;
                                        canvas.drawOval(rectF, paint);
                                    }
                                })
                        .addHighLight(
                                ResourceTable.Id_btn_bottom_light,
                                ResourceTable.Layout_info_known,
                                new OnTopPosCallback(),
                                new CircleLightShape())
                        .addHighLight(
                                view,
                                ResourceTable.Layout_info_known,
                                new OnBottomPosCallback(10),
                                new OvalLightShape(5, 5, 20))
                        .setOnRemoveCallback(
                                new HighLightInterface.OnRemoveCallback() { // 监听移除回调
                                    @Override
                                    public void onRemove() {
                                    }
                                })
                        .setOnShowCallback(
                                new HighLightInterface.OnShowCallback() { // 监听显示回调
                                    @Override
                                    public void onShow(HightLightView hightLightView) {
                                    }
                                })
                        .setOnNextCallback(
                                new HighLightInterface.OnNextCallback() {
                                    @Override
                                    public void onNext(
                                            HightLightView hightLightView, Component targetView, Component tipView) {
                                        // targetView 目标按钮 tipView添加的提示布局 可以直接找到'我知道了'按钮添加监听事件等处理
                                    }
                                })
                        .setClickCallback(
                                new HighLightInterface.OnClickCallback() {
                                    @Override
                                    public void onClick() {
                                        mHightLight.next();
                                    }
                                });
        mHightLight.show();
    }

    /**
     * 显示next模式提示布局
     *
     * @param view 被点击的视图
     * @author isanwenyu@163.com
     */
    public void showNextTipView(Component view) {
        mHightLight =
                new HighLight(MainAbilitySlice.this)
                        .anchor(rootLayout)
                        .addHighLight(
                                ResourceTable.Id_btn_right_light,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnLeftPosCallback(45),
                                new RectLightShape())
                        .addHighLight(
                                ResourceTable.Id_btn_light,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnRightPosCallback(5),
                                new CircleLightShape())
                        .addHighLight(
                                ResourceTable.Id_btn_bottom_light,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnTopPosCallback(),
                                new CircleLightShape())
                        .addHighLight(
                                view,
                                ResourceTable.Layout_info_gravity_left_down,
                                new OnBottomPosCallback(60),
                                new CircleLightShape())
                        .autoRemove(false)
                        .enableNext()
                        .setClickCallback(
                                new HighLight.OnClickCallback() {
                                    @Override
                                    public void onClick() {
                                        mHightLight.next();
                                    }
                                });
        mHightLight.show();
    }
}
